pragma solidity ^0.6.0;
pragma experimental ABIEncoderV2;

import "../LotteryGame.sol";

contract LotteryGameTest is LotteryGame {
    address public provableAddress;

    function setProvableAddress(address _provableAddress) public onlyOwner {
        provableAddress = _provableAddress;
    }

    function isProvableCallback(bytes32) internal override returns (bool) {
        return msg.sender == provableAddress;
    }
}
