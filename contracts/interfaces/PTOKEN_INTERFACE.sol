// SPDX-License-Identifier: MIT
pragma solidity ^0.6.0;

// import "@openzeppelin/contracts/token/ERC777/ERC777.sol";

interface PTOKEN_INTERFACE {
    function send(address recipient, uint256 amount, bytes calldata data) external;
    function redeem(uint256 _amount, bytes calldata _userData, string calldata _underlyingAssetRecipient) external;
}
