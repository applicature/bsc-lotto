// SPDX-License-Identifier: MIT
pragma solidity ^0.6.0;

interface TOKEN_SWAP_INTERFACE {
    function redeemOriginChainLottoTokens(
        uint256 _amount,
        address _redeemer,
        bytes calldata _userData,
        string calldata _underlyingAssetRecipient
    ) external;
}
