// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

pragma experimental ABIEncoderV2;

import "@openzeppelin/contracts-ethereum-package/contracts/access/Ownable.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/math//SafeMath.sol";
import "@openzeppelin/contracts-ethereum-packsage/contracts/utils/Address.sol";
import "./ERC20.sol";
import "./interfaces/ILotteryToken.sol";
import "./interfaces/TOKEN_SWAP_INTERFACE.sol";
import "./interfaces/ILotteryGame.sol";

contract LotteryToken is ILotteryToken, OwnableUpgradeSafe, ERC20 {
    using SafeMath for uint256;
    using Address for address;

    uint256 public constant GAMES_PER_EPOCH = 30;
    uint256 public constant PRECISION = 1_000_000_000_000_000_000;

    uint256 public constant TOTAL_ALLOCATED = 2_000_000_000;

    Epoch[] public epochs;
    Lottery[] public lotteries;

    bool public isTransferLocked;

    address [] public participants;
    mapping(address => UserBalance) public balances;

    ILotteryGame public lotteryGame;
    TOKEN_SWAP_INTERFACE public swapContract;

    modifier onlyLotteryGame() {
        require(msg.sender == address(lotteryGame), "Only allowed to call by Lottery game contract");
        _;
    }

     modifier onlySwapContract() {
        require(msg.sender == address(swapContract), "Only allowed to call by Swap contract");
        _;
    }

    function initialize(
        address _lotteryGame,
        address _swapContract
    ) public initializer {
        __ERC20_init('Lotto', 'LOTTO');
        __Ownable_init();

        lotteryGame = ILotteryGame(_lotteryGame);
        swapContract = TOKEN_SWAP_INTERFACE(_swapContract);

        epochs.push(
            Epoch(
                0, // uint256 totalFees;
                0, // uint256 minParticipationFee;
                0, // uint256 firstGameId;
                0 // uint256 lastGameId;
            )
        );

        lotteries.push(
            Lottery(
                0, // uint256 id;
                0, // uint256 participationFee;
                0, // uint256 startedAt;
                0, // uint256 finishedAt;
                0, // uint256 participants;
                address(0), // address winner;
                0, // uint256 epochId;
                0, // uint256 winningPrize;
                false // bool isActive;
            )
        );
    }

    function burn(address account, uint256 amount) override external onlySwapContract {
        _burn(account, amount);
    }

    function mint(address account, uint256 amount) override external onlySwapContract {
        _mint(account, amount);
    }

    function setSwapContract(address _swapContract) external onlyOwner {
        swapContract = TOKEN_SWAP_INTERFACE(_swapContract);
    }

    function lockTransfer() onlyLotteryGame() override public {
        isTransferLocked = true;
    }

    function unlockTransfer() onlyLotteryGame() override public {
        isTransferLocked = false;
    }

     function redeemOriginChainLottoTokens(
        uint256 _amount,
        string memory _underlyingAssetRecipient,
        bytes memory _userData
     )
        public
    {
        require(_amount <= balanceOf(msg.sender), "Insufficient balance to redeem origin chain Lotto tokens!");
        swapContract.redeemOriginChainLottoTokens(_amount, msg.sender, _userData, _underlyingAssetRecipient);
    }

    function startLottery(uint256 _participationFee) override public onlyLotteryGame() returns (Lottery memory _lotteryGame) {
        Lottery storage lottery = lotteries[lotteries.length - 1];

        // in case if prev game was not finished, halt it
        if (lottery.finishedAt == 0) {
            lottery.finishedAt = block.timestamp;
            lottery.isActive = false;
        }
        
        Epoch storage currentEpoch = epochs[epochs.length - 1];

        uint256 newGameId = lotteries.length;

        if (newGameId - currentEpoch.firstLotteryId >= GAMES_PER_EPOCH) {
            currentEpoch.lastLotteryId = lottery.id;

            epochs.push(
                Epoch(
                    0,
                    _participationFee,
                    newGameId,
                    0
                )
            );

            currentEpoch = epochs[epochs.length - 1];
        } else {
            if (currentEpoch.minParticipationFee == 0) {
                currentEpoch.minParticipationFee = _participationFee;
            }
            if (currentEpoch.minParticipationFee > _participationFee) {
                currentEpoch.minParticipationFee = _participationFee;
            }
        }

        lotteries.push(
            Lottery(
                newGameId, // uint256 id;
                _participationFee, // uint256 participationFee;
                block.timestamp, // uint256 startedAt;
                0, // uint256 finishedAt;
                0, // uint256 participants;
                address(0), // address winner;
                epochs.length - 1, // uint256 epochId;
                0, // uint256 winningPrize;
                true // bool isActive;
            )
        );

        return lotteries[lotteries.length - 1];
    }

    function finishLottery(
        uint256 _participants,
        address _winnerAddress,
        address _marketingAddress,
        uint256 _winningPrizeValue,
        uint256 _marketingFeeValue
    ) external onlyLotteryGame() override returns (Lottery memory finishedLotteryGame) {
        Lottery storage lottery = lotteries[lotteries.length - 1];

        require(lottery.isActive == true, "Lottery game is not active");

        lottery.participants = _participants;
        lottery.winner = _winnerAddress;
        lottery.winningPrize = _winningPrizeValue;

        lottery.finishedAt = block.timestamp;
        lottery.isActive = false;

        Epoch storage epoch = epochs[lottery.epochId];
        epoch.lastLotteryId = lottery.id;
        epoch.totalFees = epoch.totalFees.add(lottery.participationFee);
        
        uint256 winnerBalance = balanceOf(_winnerAddress).add(_winningPrizeValue);

        balances[_winnerAddress] = UserBalance(
            lottery.id,
            winnerBalance,
            block.timestamp
        );

        emit Transfer(address(this), _winnerAddress, _winningPrizeValue);

        uint256 marketingBalance = balanceOf(_marketingAddress).add(_marketingFeeValue);

        balances[_marketingAddress] = UserBalance(
            lottery.id,
            marketingBalance,
            block.timestamp
        );

        emit Transfer(address(this), _marketingAddress, _marketingFeeValue);
        return lottery;
    }

    function balanceOf(address account) public view override returns (uint256) {
        UserBalance storage userBalance = balances[account];

        if (lotteries.length == 0 || userBalance.balance == 0) {
            return userBalance.balance;
        }

        Lottery storage lottery = lotteries[userBalance.lastGameId];
        Epoch storage epoch = epochs[lottery.epochId];

        uint256 calculatedBalance = userBalance.balance;

        if (epoch.lastLotteryId > userBalance.lastGameId) {
            calculatedBalance = calculateBalanceAfterGames(
                calculatedBalance,
                userBalance.lastGameId + 1,
                epoch.lastLotteryId
            );
        }

        if (lottery.epochId + 1 < epochs.length) {
            for (uint256 epochId = lottery.epochId + 1; epochId < epochs.length && calculatedBalance > 0; epochId++) {
                epoch = epochs[epochId];

                if (calculatedBalance >= epoch.totalFees) {
                    calculatedBalance = calculatedBalance.sub(epoch.totalFees);
                } else {
                    if (calculatedBalance > epoch.minParticipationFee) {
                        calculatedBalance = calculateBalanceAfterGames(
                            calculatedBalance,
                            epoch.firstLotteryId,
                            epoch.lastLotteryId
                        );
                    }
                }
            }
        }

        return calculatedBalance;
    }

    function participantsCount() public view returns (uint256) {
        return participants.length;
    }

    function lotteriesCount() public view returns (uint256) {
        return lotteries.length;
    }

    function epochsCount() public view returns (uint256) {
        return epochs.length;
    }

    function lastLottery() public view override returns (Lottery memory lottery) {
        if (lotteries.length == 0) {
            return Lottery(0, 0, 0, 0, 0, address(0), 0, 0, false);
        }

        return lotteries[lotteries.length - 1];
    }

    function lastEpoch() external view override returns (Epoch memory epoch) {
        return epochs[epochs.length - 1];
    }

    function participantsBalances(uint256 from, uint256 count) public view returns (
        address [] memory participantAddresses,
        uint256 [] memory participantBalances
    ) {
        uint256 finalCount = from + count <= participants.length ? count : participants.length.sub(from);

        participantAddresses = new address[](finalCount);
        participantBalances = new uint256[](finalCount);

        for (uint256 i = from; i < from + finalCount; i++) {
            participantAddresses[i - from] = participants[i];
            participantBalances[i - from] = balanceOf(participants[i]);
        }
    }

    function calculateBalanceAfterGames(
        uint256 _calculatedBalance,
        uint256 _fromGameId,
        uint256 _toGameId
    ) internal view returns(uint256) {
        for (uint256 i = _fromGameId; i <= _toGameId && _calculatedBalance > 0; i++) {
            Lottery storage lottery = lotteries[i];
            if (_calculatedBalance >= lottery.participationFee) {
                _calculatedBalance = _calculatedBalance.sub(lottery.participationFee);
            }
        }

        return _calculatedBalance;
    }

    function _transfer(address sender, address recipient, uint256 amount) internal override(ERC20) {
        require(sender != address(0), "ERC20: transfer from the zero address");
        require(recipient != address(0), "ERC20: transfer to the zero address");
        require(!isTransferLocked, "The Game is running, all transfers are locked");

        _beforeTokenTransfer(sender, recipient, amount);

        uint256 senderBalance = balanceOf(sender).sub(amount, "ERC20: transfer amount exceeds balance");

        Lottery memory lottery = lastLottery();

        balances[sender] = UserBalance(
            lottery.id,
            senderBalance,
            block.timestamp
        );

        if (balances[recipient].at == 0 && amount > 0) {
            participants.push(recipient);
        }

        uint256 recipientBalance = balanceOf(recipient).add(amount);

        balances[recipient] = UserBalance(
            lottery.id,
            recipientBalance,
            block.timestamp
        );

        emit Transfer(sender, recipient, amount);
    }

    function _mint(address account, uint256 amount) internal override {
        require(account != address(0), "ERC20: mint to the zero address");

        uint256 balance = balanceOf(account).add(amount);

        if (balances[account].at == 0 && amount > 0) {
            participants.push(account);
        }

        Lottery memory lottery = lastLottery();

        balances[account].lastGameId = lottery.id;
        balances[account].balance = balance;
        balances[account].at = block.timestamp;
        
        _totalSupply = _totalSupply.add(amount);

        emit Transfer(address(0), account, amount);
    }

    function _burn(address account, uint256 amount) internal override {
        require(account != address(0), "ERC20: burn from the zero address");

        uint256 balance = balanceOf(account).sub(amount, "ERC20: burn amount exceeds balance");

        Lottery memory lottery = lastLottery();

        balances[account].lastGameId = lottery.id;
        balances[account].balance = balance;
        balances[account].at = block.timestamp;
        
        _totalSupply = _totalSupply.sub(amount);

        emit Transfer(account, address(0), amount);
    }
}


