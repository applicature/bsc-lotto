const LotteryToken = artifacts.require('LotteryToken');
const LotteryGame = artifacts.require('LotteryGame');
const TOKEN_SWAP = artifacts.require('TOKEN_SWAP');

const { deployProxy } = require('@openzeppelin/truffle-upgrades');
const PRECISION = "000000000000000000";

module.exports = async function (deployer) {
    const participationFee = '10'.concat(PRECISION);
    // const tokenHolder = '0x7AAE90F9fd2b6a01c1ff27cAF9265175Cbc68F07';
    const marketingFeeCollector = '0x7AAE90F9fd2b6a01c1ff27cAF9265175Cbc68F07';

    await deployProxy(LotteryToken,
        {
            deployer: deployer,
            initializer: false,
            unsafeAllowCustomTypes: true
        });
    const lotteryToken = await LotteryToken.deployed();

    await deployProxy(LotteryGame,
        {
            deployer: deployer,
            initializer: false,
            unsafeAllowCustomTypes: true
        });
    const lotteryGame = await LotteryGame.deployed();

    await deployer.deploy(TOKEN_SWAP);
    const swapContract = await TOKEN_SWAP.deployed();


    // address of our proxy contract.
    console.log('\nAddress of our proxy contract:');
    console.log('LotteryGame: ', lotteryGame.address);
    console.log('LotteryToken: ', lotteryToken.address);

    await lotteryToken.initialize(
        lotteryGame.address,
        swapContract.address
    );

    await lotteryGame.initialize(
        lotteryToken.address,
        marketingFeeCollector,
        participationFee
    );
};
