const TokenVesting = artifacts.require("TokenVesting")
const { time } = require('@openzeppelin/test-helpers');


module.exports = async function (deployer) {
   const beneficiaryTyler = "0x910266349A2aaCa44ce909b6845E8C1ab75f475E";
   const beneficiaryIan = "0x1Df55ec75809E3F81E3AD04b2bC90947a286BeCB";

   start = await Math.round(new Date() / 1000);
   cliff = await time.duration.minutes(10);
   duration = await time.duration.minutes(40);

   await deployer.deploy(TokenVesting, beneficiaryTyler, start, cliff, duration, true);

   const vestingTyler = await TokenVesting.deployed();
   console.log("Address vesting Tyler:", vestingTyler.address);

   await deployer.deploy(TokenVesting, beneficiaryIan, start, cliff, duration, true);

   const vestingIan = await TokenVesting.deployed();
   console.log("Address vesting Ian:", vestingIan.address);
}
