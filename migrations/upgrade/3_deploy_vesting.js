const TokenVesting = artifacts.require("TokenVesting")
const { time } = require('@openzeppelin/test-helpers');


module.exports = async function (deployer) {
  const beneficiaryTyler = "set address";
  const beneficiaryIan = "set address";

  let start = await Math.round(new Date() / 1000);
  let cliff = await time.duration.years(1);
  let duration = await time.duration.years(4);
  let revocable = true;

  await deployer.deploy(TokenVesting, beneficiaryTyler, start, cliff, duration, revocable);

  const vestingTyler = await TokenVesting.deployed();
  console.log("Address vesting Tyler:", vestingTyler.address);

  await deployer.deploy(TokenVesting, beneficiaryIan, start, cliff, duration, revocable);

  const vestingIan = await TokenVesting.deployed();
  console.log("Address vesting Ian:", vestingIan.address);
}