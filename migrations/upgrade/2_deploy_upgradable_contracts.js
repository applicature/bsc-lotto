const LotteryToken = artifacts.require('LotteryToken');
const LotteryGame = artifacts.require('LotteryGame');

const { deployProxy } = require('@openzeppelin/truffle-upgrades');
const PRECISION = "000000000000000000";


module.exports = async deployer => {
    const participationFee = '10'.concat(PRECISION);
    const tokenHolder = '0x910266349A2aaCa44ce909b6845E8C1ab75f475E';
    const marketingFeeCollector = '0x4dfBd2271d9273B99a62fB5d6373704b9D51E321';

    await deployProxy(LotteryToken,
        {
            deployer: deployer,
            initializer: false,
            unsafeAllowCustomTypes: true
        });
    const lotteryToken = await LotteryToken.deployed();

    await deployProxy(LotteryGame,
        {
            deployer: deployer,
            initializer: false,
            unsafeAllowCustomTypes: true
        });
    const lotteryGame = await LotteryGame.deployed();

    // address of our proxy contract.
    console.log('\nAddress of our proxy contract:');    
    console.log('LotteryGame: ', lotteryGame.address);
    console.log('LotteryToken: ', lotteryToken.address);

    await lotteryToken.initialize(
        lotteryGame.address,
        tokenHolder
    );

    await lotteryGame.initialize(
        lotteryToken.address,
        marketingFeeCollector,
        participationFee
    );
};