const LotteryGame = artifacts.require('LotteryGame');

const { upgradeProxy } = require('@openzeppelin/truffle-upgrades');

module.exports = async function (deployer) {
   const lotteryGame = "0x472F41ca844D37BEF98B4f89B47e716652247c7F";
   // const gasPrice = "160000000000";
   // const existing = await LotteryGame.deployed();
   // console.log("Existing: ", existing.address);

   console.log("Previous: ", lotteryGame);
   
   const instance = await upgradeProxy(
      lotteryGame,
      LotteryGame,
      {
         deployer: deployer,
         initializer: false,
         unsafeAllowCustomTypes: true
      }
   );
   
   //await instance.setGasPrice(gasPrice);
   console.log("Upgraded: ", instance.address);
};
