module.exports = {
  norpc: true,
  skipFiles: ['./mock/LotteryGameMock.sol',
    './mock/LotteryTokenMock.sol',
    './mock/ERC20Mock.sol',
    './mock/balance.sol',
    './mock/ERC20DecimalMock.sol',
    './game/*']
};
