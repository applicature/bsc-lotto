var HDWalletProvider = require("truffle-hdwallet-provider");
const dotenv = require('dotenv');
dotenv.config();

const MNEMONIC = 'balcony rapid toddler tunnel pyramid stay rug elder morning turn nothing attitude';
// const ETHERSCAN_KEY = 'S2MF463HSWN4PUUPUB8WMKMPVF85685Y9R'
module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",     // Localhost (default: none)
      port: 8545,            // Standard Ethereum port (default: none)
      network_id: "*",       // Any network (default: none)
      gas: 6721975,
      gasPrice: 200000000000
    },
    coverage: {
      host: "127.0.0.1",
      network_id: "*",
      port: 8545,         // <-- If you change this, also set the port option in .solcover.js.
      gas: 6721975, // <-- Use this high gas value
    },
    ropsten: {
      provider: function () {
        return new HDWalletProvider([process.env.PRIVATE_KEY], `https://ropsten.infura.io/v3/${process.env.PROJECT_ID}`)
      },
      network_id: 3,
      gas: 8000000,      //make sure this gas allocation isn't over 4M, which is the max
      gasPrice: 20000000000, // 100 gwei
      skipDryRun: true
    },
    rinkeby: {
      provider: function () {
        return new HDWalletProvider([process.env.PRIVATE_KEY], `https://rinkeby.infura.io/v3/${process.env.PROJECT_ID}`)
      },
      network_id: 4,
      gasPrice: 20000000000,
      gas: 6721975 ,     //make sure this gas allocation isn't over 4M, which is the max
      skipDryRun: true
    },

    kovan: {
      provider: function () {
        return new HDWalletProvider([process.env.PRIVATE_KEY], `https://kovan.infura.io/v3/${process.env.PROJECT_ID}`)
      },
      network_id: 42,
      gas: 6721975 ,     //make sure this gas allocation isn't over 4M, which is the max
      skipDryRun: true
    },
    mainnet: {
      provider:function () {
        return new HDWalletProvider([process.env.PRIVATE_KEY], `https://mainnet.infura.io/v3/${process.env.PROJECT_ID}`)
      },     
      network_id: 1,
      gas: 6721975,
      gasPrice: 50000000000,
      skipDryRun: true,
    },
    testnet: {
      provider: () => new HDWalletProvider([process.env.PRIVATE_KEY], `https://data-seed-prebsc-1-s1.binance.org:8545`),
      network_id: 97,
      confirmations: 10,
      timeoutBlocks: 200,
      skipDryRun: true
    },
    bsc: {
      provider: () => new HDWalletProvider([process.env.PRIVATE_KEY], `https://bsc-dataseed1.binance.org`),
      network_id: 56,
      confirmations: 10,
      timeoutBlocks: 200,
      skipDryRun: true
    },
    develop: {
      accounts: 10,
      defaultEtherBalance: 50000,
    }
  },

  mocha: {
    reporter: 'eth-gas-reporter',
    reporterOptions: {
      showTimeSpent: false,
      showMethodSig: false,
      rst: false
    },
  },
  api_keys: {
    etherscan: process.env.ETHERSCAN_KEY,
  },
  compilers: {
    solc: {
      version: "^0.6.0",
      // version: "0.5.1",    // Fetch exact version from solc-bin (default: truffle's version)
      // docker: true,        // Use "0.5.1" you've installed locally with docker (default: false)
      // settings: {          // See the solidity docs for advice about optimization and evmVersion
      optimizer: {
        enabled: true,
        runs: 200
      },
      //  evmVersion: "byzantium"
      // }
    },
  },
  plugins: ["solidity-coverage", "truffle-plugin-verify"]
};
